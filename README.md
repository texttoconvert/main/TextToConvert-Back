# TextToConvert-Back
TextToConvert back-end repository

Application is available on https://texttoconvert.herokuapp.com/

## Development guide

*Work in progress*

Pull this repository in your working folder:
```sh
git clone https://gitlab.com/texttoconvert/main/TextToConvert-Back.git
```

Go to the repository:
```sh
cd TextToConvert-Back
```

Build docker image using the development dockerfile:
```sh
docker build -f Dev.Dockerfile -t ttc .
```
You may need to launch docker commands as a sudo user if your current user is not on the docker group.
- ```-f Dev.Dockerfile``` argument is used to build the docker image using the development configuration which allow tracing, debugging and live reload. For live reload you will also need to correctly set the ```docker run``` command.
- ```-t ttc``` is used to tag the image, you can name it as you need.

Run the docker image:
```sh
docker run --mount type=bind,source="$(pwd)"/app,target=/app -p 5000:5000 ttc
```
- ```--mount type=bind,source="$(pwd)"/app,target=/app``` is used to bind, ```type=bind```, our local ```app``` development folder, ```source="$(pwd)"/app```, to the docker image ```app``` folder, ```target=/app```. You need to set this argument to allow live reload when changing local development file.
- ```-p 5000:5000``` is used to bind the docker port to your local host port allowing you to open the application on your host at http://localhost:5000/

Now enjoy and change, break, remove and add evrything you need.

Don't forget to read the License and respect author right (Just name the original TextToConvert app and me). License may change but the TextToConvert will remain an open source and free app.