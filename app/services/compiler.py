import flask
import os
import subprocess


def compile(parameters, data):
    """Compile input data and return sender with compiled file

    Keyword arguments:
    parameters -- compiling parameters (dict):
        {
            "inputFormat": Input format,
            "outputFormat": Format of the compiled file
        }
    data -- data to compile (dict):
        {
            "input": The data to compile
        }

    Return:
    Compiled file
    """
    writeInput(parameters.get("inputFormat"), data.get("input"))
    convert(parameters)
    return flask.send_from_directory("tmp",
                                     "tempOutput." + parameters.get("outputFormat"), as_attachment=False)


def writeInput(inputFormat, input):
    """Write input to file on the wanted format

    Keyword arguments:
    inputFormat -- the input format, also the format of the file (string)
    input -- the input data to write down on the file (string)
    """
    with open("./tmp/tempInput." + inputFormat, "w") as tempInput:
        print(input)
        tempInput.write(input)


def convert(parameters):
    """Convert a file to another format with the given parameters

    Keyword arguments:
    parameters -- converter parameters:
        {
            "inputFormat": Input data format,
            "outputFormat": Format of the converted file
        }
    """
    subprocess.run(["pandoc", "./tmp/tempInput." + parameters.get("inputFormat"),
                    "-o", "./tmp/tempOutput." + parameters.get("outputFormat")], check=True)
