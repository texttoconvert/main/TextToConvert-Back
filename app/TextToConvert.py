from flask import Flask
from flask import request
from flask import json
from flask_cors import CORS, cross_origin
from sentry_sdk.integrations.flask import FlaskIntegration
import subprocess
import services.compiler as Compiler
import sentry_sdk

sentry_sdk.init(
    dsn="https://ce22751d658947c0bea92b058072d970@sentry.io/1804312",
    integrations=[FlaskIntegration()]
)

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route("/")
@cross_origin()
def hello():
    return "Welcome to TextToConvert API!"


@app.route("/compile", methods=['POST'])
@cross_origin()
def compile():
    """Compile input data to the wanted format

    Method: POST
    """
    return Compiler.compile(request.json.get("parameters"), request.json.get("data"))

@app.route('/debug-sentry')
def trigger_error():
    division_by_zero = 1 / 0

if __name__ == "__main__":
    app.run(host='0.0.0.0')
