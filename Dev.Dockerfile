# Use an Python docker image with Pandoc and Texlive on it
FROM registry.gitlab.com/texttoconvert/tools/python-pandoc-texlive:latest

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY ./app /app

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# For dev purpose
ENV FLASK_DEBUG=1
ENV FLASK_ENV=development
ENV FLASK_APP=TextToConvert.py

# Run the app.  CMD is required to run on Heroku
# $PORT is set by Heroku
CMD flask run --host=0.0.0.0

